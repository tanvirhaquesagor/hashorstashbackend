-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 17, 2018 at 04:37 AM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hashorstash`
--

-- --------------------------------------------------------

--
-- Table structure for table `hashes_and_stashes`
--

CREATE TABLE `hashes_and_stashes` (
  `id` int(10) NOT NULL,
  `userid` varchar(100) DEFAULT NULL,
  `comments` varchar(100) DEFAULT NULL,
  `cmtTime` bigint(20) DEFAULT NULL,
  `location` varchar(100) DEFAULT NULL,
  `locationId` varchar(100) DEFAULT NULL,
  `latitute` double DEFAULT NULL,
  `longitute` double DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL,
  `duration` bigint(10) DEFAULT NULL,
  `hashOrStash` varchar(10) DEFAULT NULL,
  `voteCount` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `hashes_and_stashes`
--

INSERT INTO `hashes_and_stashes` (`id`, `userid`, `comments`, `cmtTime`, `location`, `locationId`, `latitute`, `longitute`, `image`, `duration`, `hashOrStash`, `voteCount`) VALUES
(30, '0', 'good', 1541612319947, 'Dhaka Bank Limited', '3919375214', 23.7864954, 90.3295159, NULL, 60000, 'hash', 0),
(32, '0', 'lll', 1541613528128, 'Paturia Ferry Terminal', '2428945004', 23.7859976, 89.8117862, NULL, 60000, 'hash', 0),
(33, '0', 'love', 1541613621502, 'BADC High School', '2715213686', 23.7864729, 90.3515312, NULL, 86400000, 'hash', 0),
(38, '3', '#mosque', 1541702669042, 'Govt B L College Mosque', '2971222733', 22.8681994, 89.528803, NULL, 2700000, 'hash', 0),
(39, '3', '#college,khulna', 1541704112242, 'B.N College Khulna', '1028350774', 22.8509383, 89.5298469, NULL, 3600000, 'hash', 0),
(40, '3', '#college,khulna', 1541704188383, 'B.N College Khulna', '1028350774', 22.8509383, 89.5298469, NULL, 2700000, 'hash', 0),
(41, '0', '# BNcollege', 1541704998286, 'B.N College Khulna', '1028350774', 22.8509383, 89.5298469, NULL, 2100000, 'hash', 0),
(43, '0', '# BNcollege,khulna', 1541705027303, 'B.N College Khulna', '1028350774', 22.8509383, 89.5298469, NULL, 3600000, 'hash', 0),
(44, '3', '#noakhali', 1541705741149, 'Noakhali Govt Girls High School', '2345936713', 22.8656711, 91.1019127, NULL, 1500000, 'hash', 0),
(45, '3', '#mosque,noakhali', 1541706706572, 'Noakhali Zila Jama Masjid', '835168094', 22.864908, 91.098666, NULL, 1500000, 'hash', 0),
(46, '3', '#mosque,noakhali,,,stash', 1541706716319, 'Noakhali Zila Jama Masjid', '835168094', 22.864908, 91.098666, NULL, 3600000, 'stash', 0),
(48, '3', '#highschoolnoakhali', 1541709685723, 'Noakhali Govt Girls High School', '2345936713', 22.8656711, 91.1019127, NULL, 3600000, 'hash', 0),
(49, '3', '#bestschoolinnoakhali', 1541709908746, 'Noakhali Govt Girls High School', '2345936713', 22.8656711, 91.1019127, NULL, 2700000, 'hash', 0),
(51, '3', '#picofschool', 1541710198504, 'Noakhali Govt Girls High School', '2345936713', 22.8656711, 91.1019127, NULL, 3600000, 'hash', 0),
(52, '3', '#wellknowedcollege', 1541736622156, 'B.N College Khulna', '1028350774', 22.8509383, 89.5298469, NULL, 3600000, 'hash', 0),
(53, '3', 'ghgigo', 1541878701411, 'Noakhali Zila Jama Masjid', '835168094', 22.864908, 91.098666, NULL, 3600000, 'hash', 0),
(54, '3', 'jamemosque', 1541911640278, 'Noakhali Zila Jama Masjid', '835168094', 22.864908, 91.098666, NULL, 2100000, 'hash', 0),
(55, '3', '#prayertime', 1541914298725, 'Noakhali Zila Jama Masjid', '835168094', 22.864908, 91.098666, NULL, 1500000, 'hash', 0),
(56, '3', '#this value is 35 min', 1541915274275, 'Noakhali Zila Jama Masjid', '835168094', 22.864908, 91.098666, NULL, 2100000, 'hash', 0),
(57, '3', '#this value is 45 min', 1541915302684, 'Noakhali Zila Jama Masjid', '835168094', 22.864908, 91.098666, NULL, 2700000, 'hash', 0),
(58, '3', '#prayertime2', 1541918293639, 'Noakhali Zila Jama Masjid', '835168094', 22.864908, 91.098666, NULL, 900000, 'hash', 0),
(63, '3', 'hello', 1541920044619, 'B.N College Khulna', '1028350774', 22.8509383, 89.5298469, NULL, 1500000, 'hash', 3),
(64, '3', '#jamemosjid', 1541956891743, 'Noakhali Zila Jama Masjid', '835168094', 22.864908, 91.098666, NULL, 900000, 'hash', 0),
(65, '3', '#good college ', 1541957036059, 'B.N College Khulna', '1028350774', 22.8509383, 89.5298469, NULL, 900000, 'hash', 0),
(66, '3', '#bestschool', 1541957685776, 'Noakhali Govt Girls High School', '2345936713', 22.8656711, 91.1019127, NULL, 900000, 'hash', 1);

-- --------------------------------------------------------

--
-- Table structure for table `hash_vote`
--

CREATE TABLE `hash_vote` (
  `id` int(255) NOT NULL,
  `hash_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `hash_vote`
--

INSERT INTO `hash_vote` (`id`, `hash_id`, `user_id`) VALUES
(6, '3', '3'),
(26, '24', '1'),
(27, '24', '0'),
(31, '28', '0'),
(32, '34', '1'),
(37, '34', '1'),
(41, '37', '3'),
(42, '38', '3'),
(43, '39', '0'),
(44, '41', '3'),
(49, '49', '3'),
(50, '51', '3'),
(69, '43', '3'),
(70, '52', '3'),
(77, '39', '3'),
(87, '54', '3'),
(88, '63', '2');

--
-- Triggers `hash_vote`
--
DELIMITER $$
CREATE TRIGGER `deleteVote` AFTER DELETE ON `hash_vote` FOR EACH ROW UPDATE hashes_and_stashes set voteCount= voteCount-1 where hashes_and_stashes.id  = (old.hash_id)
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `vote_count` AFTER INSERT ON `hash_vote` FOR EACH ROW UPDATE hashes_and_stashes set voteCount= voteCount+1 where hashes_and_stashes.id  = (new.hash_id)
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `profile`
--

CREATE TABLE `profile` (
  `id` int(10) NOT NULL,
  `name` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` mediumtext COLLATE utf8mb4_unicode_ci,
  `image` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `profile`
--

INSERT INTO `profile` (`id`, `name`, `username`, `password`, `phone`, `email`, `country`, `image`) VALUES
(1, 'Tanvir', 'Default_username', 'Default_password', '01703889397', 'default_email@gmail.com', 'Bangladesg ', '1536252583659.jpg'),
(2, 'Md. Atikur Rahman', 'Default_username', 'Default_password', 'Default_mobile', 'atikur11@cse.pstu.ac.bd', 'USA', 'demouser.png'),
(3, 'Atik Alif', 'Default_username', 'Default_password', 'Default_mobile', 'atikalif007@gmail.com', 'USA', 'demouser.png'),
(4, 'Default_name', 'Default_username', 'Default_password', 'Default_mobile', 'timitbd5@gmail.com', 'USA', 'demouser.png'),
(5, 'Default_name', 'Default_username', 'Default_password', '01751289472', 'default_email@gmail.com', 'USA', 'demouser.png');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `hashes_and_stashes`
--
ALTER TABLE `hashes_and_stashes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hash_vote`
--
ALTER TABLE `hash_vote`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profile`
--
ALTER TABLE `profile`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `hashes_and_stashes`
--
ALTER TABLE `hashes_and_stashes`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT for table `hash_vote`
--
ALTER TABLE `hash_vote`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;

--
-- AUTO_INCREMENT for table `profile`
--
ALTER TABLE `profile`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
