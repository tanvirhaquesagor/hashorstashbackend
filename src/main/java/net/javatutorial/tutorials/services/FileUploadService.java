/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.javatutorial.tutorials.services;

import com.google.gson.Gson;
import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;
import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataParam;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

/**
 * This example shows how to build Java REST web-service to upload files
 * accepting POST requests with encoding type "multipart/form-data". For more
 * details please read the full tutorial on
 * https://javatutorial.net/java-file-upload-rest-service
 *
 * @author javatutorial.net
 */
@Path("/upload")
public class FileUploadService {

    private static String UPLOAD_FOLDER = "res/image";

    public FileUploadService() {
    }
    @Context
    private UriInfo context;

    @GET
    @Path("/profile/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public String sayHelloJSON(@PathParam("id") String id) {
        User usr = new User();
        //  
        //  ="1234567890";
        ArrayList<User> userlist = new ArrayList<User>();
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection con;
             con = (Connection) DriverManager.getConnection("jdbc:mysql://139.59.74.201/hashorstash", "tanvir", "De_pw9");
            Statement stmt = (Statement) con.createStatement();
            ResultSet rs = stmt.executeQuery("select * from profile where email='" + id + "' or phone='" + id + "'");

            while (rs.next()) {
                usr.setId(rs.getInt(1));
                usr.setName(rs.getString(2));
                usr.setUsername(rs.getString(3));
                usr.setPassword(rs.getString(4));
                usr.setPhone(rs.getString(5));
                usr.setEmail(rs.getString(6));
                usr.setCountry(rs.getString(7));
                usr.setImage(rs.getString(8));
                userlist.add(usr);
            }
        } catch (Exception e) {
            System.out.println(e);
        }

        return new Gson().toJson(userlist);
        // return userlist;
    }

    @GET
    @Path("/user/{name}/{username}/{password}/{phone}/{email}/{country}/{image}")
    @Produces(MediaType.APPLICATION_JSON)
    public String sayUserJSON(@PathParam("name") String name,
            @PathParam("username") String username,
            @PathParam("password") String password,
            @PathParam("phone") String phone,
            @PathParam("email") String email,
            @PathParam("country") String country,
            @PathParam("image") String image) {
        //  User usr = new User();
        //  
        //  ="1234567890";
        String result = "";
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection con;
             con = (Connection) DriverManager.getConnection("jdbc:mysql://139.59.74.201/hashorstash", "tanvir", "De_pw9");
            Statement stmt1 = (Statement) con.createStatement();

            String sq = "select * from profile where phone='" + phone + "'";
            if (!phone.equals("Default_mobile")) {
                sq = "select * from profile where phone='" + phone + "'";
            } else if (!email.equals("default_email@gmail.com")) {
                sq = "select * from profile where email='" + email + "'";
            }

            ResultSet rs = stmt1.executeQuery(sq);

            if (!rs.next()) {
                Statement stmt = (Statement) con.createStatement();
                String query = "INSERT INTO profile (name,username,password,phone,email,country,image)  VALUES (?, ?, ?, ?, ?, ?, ?)";

                PreparedStatement preparedStmt = con.prepareStatement(query);
                preparedStmt.setString(1, name);
                preparedStmt.setString(2, username);
                preparedStmt.setString(3, password);
                preparedStmt.setString(4, phone);
                preparedStmt.setString(5, email);
                preparedStmt.setString(6, country);
                preparedStmt.setString(7, image);
                preparedStmt.execute();
                result = "success";
            } else {
                result = "already";
            }

            con.close();
        } catch (Exception e) {
            result = "failed";
            System.out.println(e);
        }
        return new Gson().toJson(result);
        // return userlist;
    }

    @GET
    @Path("/userupdate/{name}/{username}/{password}/{phone}/{email}/{country}/{image}")
    @Produces(MediaType.APPLICATION_JSON)
    public String sayUserUpdateJSON(@PathParam("name") String name,
            @PathParam("username") String username,
            @PathParam("password") String password,
            @PathParam("phone") String phone,
            @PathParam("email") String email,
            @PathParam("country") String country,
            @PathParam("image") String image) {
        String result = "";
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection con;
            con = (Connection) DriverManager.getConnection("jdbc:mysql://139.59.74.201/hashorstash", "tanvir", "De_pw9");
            String query = "UPDATE profile SET name=?, username=?, password=?, phone=?, email=?, country=?, image=? WHERE phone='" + phone + "'";
            if (!phone.equals("Default_mobile")) {
                query = "UPDATE profile SET name=?, username=?, password=?, phone=?, email=?, country=?, image=? WHERE phone='" + phone + "'";
            } else if (!email.equals("default_email@gmail.com")) {
                query = "UPDATE profile SET name=?, username=?, password=?, phone=?, email=?, country=?, image=? WHERE email='" + email + "'";
            }
            PreparedStatement preparedStmt = con.prepareStatement(query);
            preparedStmt.setString(1, name);
            preparedStmt.setString(2, username);
            preparedStmt.setString(3, password);
            preparedStmt.setString(4, phone);
            preparedStmt.setString(5, email);
            preparedStmt.setString(6, country);
            preparedStmt.setString(7, image);
            preparedStmt.executeUpdate();
            result = "success";
            con.close();
        } catch (Exception e) {
            result = "failed";
            System.out.println(e);
        }
        return new Gson().toJson(result);

    }

    public String getPath() throws UnsupportedEncodingException {
        String path = this.getClass().getClassLoader().getResource("").getPath();
        String fullPath = URLDecoder.decode(path, "UTF-8");
        String pathArr[] = fullPath.split("/target/");
        System.out.println(fullPath);
        System.out.println(pathArr[0]);
///C:/Users/sagor/Documents/NetBeansProjects/mavenFileUpload/target/mavenFileUpload-1.0-SNAPSHOT/WEB-INF/classes/
//C:\Users\sagor\Documents\NetBeansProjects\mavenFileUpload\src\main\webapp\images
        fullPath = pathArr[0];
        String reponsePath = "";
// to read a file from webcontent
        reponsePath = new File(fullPath).getPath() + File.separatorChar + "\\src\\main\\webapp\\images\\";
        return reponsePath;
    }

    @POST
    @Path("/image")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    public String uploadFile(
            @FormDataParam("file") InputStream uploadedInputStream,
            @FormDataParam("file") FormDataContentDisposition fileDetail,
            @FormDataParam("path") String path,
            @FormDataParam("name") String name,
            @FormDataParam("phone") String phone,
            @FormDataParam("email") String email,
            @FormDataParam("country") String country
    ) throws UnsupportedEncodingException {

        path = path.replace("\"", "");
        name = name.replace("\"", "");
        phone = phone.replace("\"", "");
        email = email.replace("\"", "");
        country = country.replace("\"", "");

        String result = "failed";
        // check if all form parameters are provided
        if (uploadedInputStream == null || fileDetail == null) {
            //   return Response.status(400).entity("Invalid form data").build();
            return new Gson().toJson(result);
        }
        // create our destination folder, if it not exists
        try {
            //Relative to absulate
            //      UPLOAD_FOLDER = getPath();
            UPLOAD_FOLDER = "//opt//tomcat//webapps//mavenFileUpload//images//";

            createFolderIfNotExists(UPLOAD_FOLDER);

        } catch (SecurityException se) {
//            return Response.status(500)
//                    .entity("Can not create destination folder on server")
//                    .build();
            return new Gson().toJson(result);

        }
        // String uploadedFileLocation = UPLOAD_FOLDER + "//" + fileDetail.getFileName();
        String uploadedFileLocation = UPLOAD_FOLDER + path.toString();
        try {
            saveToFile(uploadedInputStream, uploadedFileLocation);
            //Update information
            Class.forName("com.mysql.jdbc.Driver");
            Connection con;
          con = (Connection) DriverManager.getConnection("jdbc:mysql://139.59.74.201/hashorstash", "tanvir", "De_pw9");
            String query = "UPDATE profile SET name=?  , phone=?, email=?, country=?, image=? WHERE phone='" + phone + "'";
            if (!phone.equals("Default_mobile")) {
                query = "UPDATE profile SET name=?,   phone=?, email=?, country=?, image=? WHERE phone='" + phone + "'";
            } else if (!email.equals("default_email@gmail.com")) {
                query = "UPDATE profile SET name=?,   phone=?, email=?, country=?, image=? WHERE email='" + email + "'";
            }
            PreparedStatement preparedStmt = con.prepareStatement(query);
            preparedStmt.setString(1, name);
            preparedStmt.setString(2, phone);
            preparedStmt.setString(3, email);
            preparedStmt.setString(4, country);
            preparedStmt.setString(5, path);
            preparedStmt.executeUpdate();
            result = "success";
            con.close();
        } catch (Exception e) {
//            return Response.status(500).entity("Can not save file" + getPath()).build();
            return new Gson().toJson(result + e.toString());
        }
//        return Response.status(200)
//                .entity("File saved to " + uploadedFileLocation).build();
        return new Gson().toJson(result);
    }

    /**
     * Utility method to save InputStream data to target location/file
     *
     * @param inStream - InputStream to be saved
     * @param target - full path to destination file
     */
    private void saveToFile(InputStream inStream, String target)
            throws IOException {
        OutputStream out = null;
        int read = 0;
        byte[] bytes = new byte[1024];
        out = new FileOutputStream(new File(target));
        while ((read = inStream.read(bytes)) != -1) {
            out.write(bytes, 0, read);
        }
        out.flush();
        out.close();
    }

    //Save Hash or Stash--------------------------------------------------------
    @GET
    @Path("/savehashorstash/{userid}/{comments}/{cmtTime}/{location}/{locationId}/{latitute}/{longitute}/{duration}/{hashOrStash}")
    @Produces(MediaType.APPLICATION_JSON)
    public String SaveHashOrStash(@PathParam("userid") String userid,
            @PathParam("comments") String comments,
            @PathParam("cmtTime") long cmtTime,
            @PathParam("location") String location,
            @PathParam("locationId") String locationId,
            @PathParam("latitute") String latitute,
            @PathParam("longitute") String longitute,
            @PathParam("duration") long duration,
            @PathParam("hashOrStash") String hashOrStash) {
        String result = "";
        try {
            Calendar calendar=Calendar.getInstance();
            Date date=calendar.getTime();
            long dateTime = calendar.getTimeInMillis();
            Class.forName("com.mysql.jdbc.Driver");
            Connection con;
            con = (Connection) DriverManager.getConnection("jdbc:mysql://139.59.74.201/hashorstash", "tanvir", "De_pw9");
            String query = "INSERT INTO `hashes_and_stashes` ( `userid`, `comments`, `cmtTime`, `location`, `locationId`, `latitute`, `longitute`, `duration`, `hashOrStash`) VALUES (?, ?, ?, ?, ?, ?, ?,?,?)";

            PreparedStatement preparedStmt = con.prepareStatement(query);
            preparedStmt.setString(1, userid);
            preparedStmt.setString(2, comments);
            preparedStmt.setLong(3, dateTime);
            preparedStmt.setString(4, location);
            preparedStmt.setString(5, locationId);
            preparedStmt.setDouble(6, Double.parseDouble(latitute));
            preparedStmt.setDouble(7, Double.parseDouble(longitute));
            preparedStmt.setLong(8, duration);
            preparedStmt.setString(9, hashOrStash);

            preparedStmt.execute();
            result = "success";
            con.close();
        } catch (Exception e) {
            result = "failed";
            System.out.println(e);
        }
        return new Gson().toJson(result);
    }

    //------------------------------------------------------------------------
    ///   Hash Data fetch.......................................................
    @GET
    @Path("/hashfetch")// Ekhane muloto lat long asbe +location id o aste pare .....tar upore depend kore information asbe....!
    @Produces(MediaType.APPLICATION_JSON)
    public String hashfetch() {
        HashStashModel hash = null;
        ResultSet rs = null;
        //  
        //  ="1234567890";
        ArrayList<HashStashModel> hashStashList = new ArrayList<HashStashModel>();
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            Connection con = null;
             con = (Connection) DriverManager.getConnection("jdbc:mysql://139.59.74.201/hashorstash", "tanvir", "De_pw9");
            Statement stmnt = (Statement) con.createStatement();

            rs = stmnt.executeQuery("SELECT * FROM hashes_and_stashes WHERE hashOrStash='hash'");

            while (rs.next()) {
                hash = new HashStashModel();
                hash.setRowId(rs.getInt("id"));
                hash.setUserId(rs.getString("userid"));
                hash.setComments(rs.getString("comments"));
                hash.setCmtTime(rs.getLong("cmtTime"));
                hash.setLocation(rs.getString("location"));
                hash.setLocationId(rs.getString("locationId"));
                hash.setLatitute(rs.getString("latitute"));
                hash.setLongitute(rs.getString("longitute"));
                hash.setImage(rs.getString("image"));
                hash.setDuration(rs.getInt("duration"));
                hash.setHashOrStash(rs.getString("hashOrStash"));
                hash.setVotecount(rs.getInt("voteCount"));
                hashStashList.add(hash);
            }
//            con.close();
        } catch (Exception e) {
            System.out.println(e);
        }

        return new Gson().toJson(hashStashList);
        // return userlist;
    }
//..............................................................

    @GET
    @Path("/gethashwithinkm/{latitute}/{longitute}")// Ekhane muloto lat long asbe +location id o aste pare .....tar upore depend kore information asbe....!
    @Produces(MediaType.APPLICATION_JSON)
    public String hashfetchwithinkm(@PathParam("latitute") String latitute,
            @PathParam("longitute") String longitute) {
        HashStashModel hash = null;
        ResultSet rs = null;
        //  
        //  ="1234567890";

        Double Lat = Double.parseDouble(latitute);
        Double Lon = Double.parseDouble(longitute);

        Double latmin, longmin, latmax, longmax;
        double N = 3000;

        latmax = Lat + (0.009 * N);
        latmin = Lat - (0.009 * N);
        longmax = Lon + (0.009 * N);
        longmin = Lon - (0.009 * N);

        ArrayList<HashStashModel> hashStashList = new ArrayList<HashStashModel>();
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            Connection con = null;
           con = (Connection) DriverManager.getConnection("jdbc:mysql://139.59.74.201/hashorstash", "tanvir", "De_pw9");
            Calendar calendar=Calendar.getInstance();
            Date date=calendar.getTime();
            long dateTime = calendar.getTimeInMillis();
            String quary = "SELECT * FROM hashes_and_stashes WHERE latitute>=? and latitute<=? and longitute>=? and longitute<=? and ((cmtTime+duration)>=? or voteCount>0)";
            PreparedStatement stmnt = (PreparedStatement) con.prepareStatement(quary);
            stmnt.setDouble(1, latmin);
            stmnt.setDouble(2, latmax);
            stmnt.setDouble(3, longmin);
            stmnt.setDouble(4, longmax);     
            stmnt.setLong(5, dateTime);

            rs = stmnt.executeQuery();
            

            while (rs.next()) {
                hash = new HashStashModel();
                hash.setRowId(rs.getInt("id"));
                hash.setUserId(rs.getString("userid"));
                hash.setComments(rs.getString("comments"));
                hash.setCmtTime(rs.getLong("cmtTime"));
                hash.setLocation(rs.getString("location"));
                hash.setLocationId(rs.getString("locationId"));
                hash.setLatitute(rs.getString("latitute"));
                hash.setLongitute(rs.getString("longitute"));
                hash.setImage(rs.getString("image"));
                hash.setDuration(rs.getInt("duration"));
                hash.setHashOrStash(rs.getString("hashOrStash"));
                 hash.setVotecount(rs.getInt("voteCount"));
                hashStashList.add(hash);
            }
//            con.close();
        } catch (Exception e) {
            System.out.println(e);
        }

        return new Gson().toJson(hashStashList);
        // return userlist;
    }
    
    
     @GET
    @Path("/gethashbylocationid/{locationid}")// Ekhane muloto lat long asbe +location id o aste pare .....tar upore depend kore information asbe....!
    @Produces(MediaType.APPLICATION_JSON)
    public String hashfetchwithinkm(@PathParam("locationid") String locationid ) {
        HashStashModel hash = null;
        ResultSet rs = null;
      

        ArrayList<HashStashModel> hashStashList = new ArrayList<HashStashModel>();
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            Connection con = null;
            con = (Connection) DriverManager.getConnection("jdbc:mysql://139.59.74.201/hashorstash", "tanvir", "De_pw9");
         Calendar calendar=Calendar.getInstance();
            Date date=calendar.getTime();
            long dateTime = calendar.getTimeInMillis();
            String quary = "SELECT * FROM hashes_and_stashes WHERE locationId=? and hashOrStash='hash' and ((cmtTime+duration)>=? or voteCount>0)";
            PreparedStatement stmnt = (PreparedStatement) con.prepareStatement(quary);
            stmnt.setString(1, locationid);
            stmnt.setLong(2, dateTime);
            rs = stmnt.executeQuery();

            while (rs.next()) {
                hash = new HashStashModel();
                hash.setRowId(rs.getInt("id"));
                hash.setUserId(rs.getString("userid"));
                hash.setComments(rs.getString("comments"));
                hash.setCmtTime(rs.getLong("cmtTime"));
                hash.setLocation(rs.getString("location"));
                hash.setLocationId(rs.getString("locationId"));
                hash.setLatitute(rs.getString("latitute"));
                hash.setLongitute(rs.getString("longitute"));
                hash.setImage(rs.getString("image"));
                hash.setDuration(rs.getInt("duration"));
                hash.setHashOrStash(rs.getString("hashOrStash"));
                hash.setVotecount(rs.getInt("voteCount"));
                hashStashList.add(hash);
            }
//            con.close();
        } catch (Exception e) {
            System.out.println(e);
        }

        return new Gson().toJson(hashStashList);
        // return userlist;
    }
    
    
    
    
     @GET
    @Path("/getstashbylocationid/{locationid}/{id}")// Ekhane muloto lat long asbe +location id o aste pare .....tar upore depend kore information asbe....!
    @Produces(MediaType.APPLICATION_JSON)
    public String stashfetchwithinkm(@PathParam("locationid") String locationid,
            @PathParam("id") String id) {
        HashStashModel hash = null;
        ResultSet rs = null;
      

        ArrayList<HashStashModel> hashStashList = new ArrayList<HashStashModel>();
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            Connection con = null;
            con = (Connection) DriverManager.getConnection("jdbc:mysql://139.59.74.201/hashorstash", "tanvir", "De_pw9");
         
            String quary = "SELECT * FROM hashes_and_stashes WHERE locationId=? and hashOrStash='stash' and userid='" + id + "'";
            PreparedStatement stmnt = (PreparedStatement) con.prepareStatement(quary);
            stmnt.setString(1, locationid);

            rs = stmnt.executeQuery();

            while (rs.next()) {
                hash = new HashStashModel();
                hash.setRowId(rs.getInt("id"));
                hash.setUserId(rs.getString("userid"));
                hash.setComments(rs.getString("comments"));
                hash.setCmtTime(rs.getLong("cmtTime"));
                hash.setLocation(rs.getString("location"));
                hash.setLocationId(rs.getString("locationId"));
                hash.setLatitute(rs.getString("latitute"));
                hash.setLongitute(rs.getString("longitute"));
                hash.setImage(rs.getString("image"));
                hash.setDuration(rs.getInt("duration"));
                hash.setHashOrStash(rs.getString("hashOrStash"));
                 hash.setVotecount(rs.getInt("voteCount"));
                hashStashList.add(hash);
            }
//            con.close();
        } catch (Exception e) {
            System.out.println(e);
        }

        return new Gson().toJson(hashStashList);
        // return userlist;
    }
    
    
    //..........................................................................
    ///   Stash Data fetch.......................................................

    @GET
    @Path("/stashfetch/{id}")// Ekhane muloto lat long asbe +location id o aste pare .....tar upore depend kore information asbe....!
    @Produces(MediaType.APPLICATION_JSON)
    public String stashfetch(@PathParam("id") String id) {
        HashStashModel hash = null;
        ResultSet rs = null;
        //  
        //  ="1234567890";
        ArrayList<HashStashModel> hashStashList = new ArrayList<HashStashModel>();
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            Connection con = null;
            con = (Connection) DriverManager.getConnection("jdbc:mysql://139.59.74.201/hashorstash", "tanvir", "De_pw9");
            Statement stmnt = (Statement) con.createStatement();

            rs = stmnt.executeQuery("SELECT * FROM hashes_and_stashes WHERE hashOrStash='stash' and userid='" + id + "'");

            while (rs.next()) {
                hash = new HashStashModel();
                hash.setRowId(rs.getInt("id"));
                hash.setUserId(rs.getString("userid"));
                hash.setComments(rs.getString("comments"));
                hash.setCmtTime(rs.getLong("cmtTime"));
                hash.setLocation(rs.getString("location"));
                hash.setLocationId(rs.getString("locationId"));
                hash.setLatitute(rs.getString("latitute"));
                hash.setLongitute(rs.getString("longitute"));
                hash.setImage(rs.getString("image"));
                hash.setDuration(rs.getInt("duration"));
                hash.setHashOrStash(rs.getString("hashOrStash"));
                 hash.setVotecount(rs.getInt("voteCount"));
                hashStashList.add(hash);
            }
//            con.close();
        } catch (Exception e) {
            System.out.println(e);
        }

        return new Gson().toJson(hashStashList);
        // return userlist;
    }

    //..........................................................................
    //Save Hash Vote--------------------------------------------------------
    @GET
    @Path("/savehashvote/{hash_id}/{user_id}")
    @Produces(MediaType.APPLICATION_JSON)
    public String SaveHashVote(
            @PathParam("hash_id") String hash_id,
            @PathParam("user_id") String user_id) {
        String result = "";
        ResultSet rs = null;
        String data = "";
        PreparedStatement preparedStmt = null;
        int i = 0;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection con;
            con = (Connection) DriverManager.getConnection("jdbc:mysql://139.59.74.201/hashorstash", "tanvir", "De_pw9");
            //String query = "INSERT INTO hash_vote(hash_id,user_id) VALUES (?,?)";

            Statement stmnt = (Statement) con.createStatement();

            rs = stmnt.executeQuery("SELECT hash_id FROM hash_vote WHERE  user_id='" + user_id + "' and hash_id='" + hash_id + "'");
            while (rs.next()) {

                data = rs.getString(1);

                if (data.equals(hash_id)) {
                    i = 1;

                } else {
                    i = 0;
                }
            }
            if (i == 1) {
                result = "already inserted";
            } else {
                preparedStmt = con.prepareStatement("INSERT INTO hash_vote(hash_id,user_id) VALUES (?,?)");
                preparedStmt.setString(1, hash_id);
                preparedStmt.setString(2, user_id);
                preparedStmt.execute();
                result = "success";
            }

            //con.close();
        } catch (Exception e) {
            result = "failed";
            System.out.println(e);
        }
        return new Gson().toJson(result);
    }

    //------------------------------------------------------------------------
    //Delete Hash Vote--------------------------------------------------------
    @GET
    @Path("/deletehashvote/{hash_id}/{user_id}")
    @Produces(MediaType.APPLICATION_JSON)
    public String DeleteHashVote(
            @PathParam("hash_id") String hash_id,
            @PathParam("user_id") String user_id) {
        String result = "";
        ResultSet rss = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection con;
             con = (Connection) DriverManager.getConnection("jdbc:mysql://139.59.74.201/hashorstash", "tanvir", "De_pw9");
            String query = "DELETE FROM hash_vote WHERE hash_id=? and user_id=?";
            PreparedStatement preparedStmt = con.prepareStatement(query);
            preparedStmt.setString(1, hash_id);
            preparedStmt.setString(2, user_id);
            // execute the preparedstatement 
            preparedStmt.execute();

            result = "success";
            con.close();
        } catch (Exception e) {
            result = "failed";
            System.out.println(e);
        }
        return new Gson().toJson(result);
    }

    //------------------------------------------------------------------------
    //Delete Stash --------------------------------------------------------
    @GET
    @Path("/deletestash/{stash_id}/{user_id}")
    @Produces(MediaType.APPLICATION_JSON)
    public String DeleteStash(
            @PathParam("stash_id") String stash_id,
            @PathParam("user_id") String user_id) {
        String result = "";

        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            Connection con = null;
             con = (Connection) DriverManager.getConnection("jdbc:mysql://139.59.74.201/hashorstash", "tanvir", "De_pw9");

            String query = "DELETE FROM hashes_and_stashes WHERE  userid=? and id=?";
            PreparedStatement preparedStmt = con.prepareStatement(query);
            preparedStmt.setString(1, user_id);
            preparedStmt.setString(2, stash_id);
            // execute the preparedstatement 
            preparedStmt.execute();

            result = "success";
            con.close();
        } catch (Exception e) {
            result = "failed";
            System.out.println(e);
        }
        return new Gson().toJson(result);
    }

    //------------------------------------------------------------------------
    ///   Voting Data fetch.......................................................
    @GET
    @Path("/votedatafetch/{user_id}")// Ekhane muloto lat long asbe +location id o aste pare .....tar upore depend kore information asbe....!
    @Produces(MediaType.APPLICATION_JSON)
    public String votedatafetch(@PathParam("user_id") String user_id) {
        VotingDataModel vote = null;
        ResultSet rs = null;
        //  
        //  ="1234567890";
        ArrayList<VotingDataModel> voteList = new ArrayList<VotingDataModel>();
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            Connection con = null;
            con = (Connection) DriverManager.getConnection("jdbc:mysql://139.59.74.201/hashorstash", "tanvir", "De_pw9");
            Statement stmnt = (Statement) con.createStatement();

            rs = stmnt.executeQuery("SELECT hash_id FROM hash_vote WHERE  user_id='" + user_id + "'");

            while (rs.next()) {
                vote = new VotingDataModel();

                vote.setHashId(rs.getString("hash_id"));

                voteList.add(vote);
            }
//            con.close();
        } catch (Exception e) {
            System.out.println(e);
        }

        return new Gson().toJson(voteList);
        // return userlist;
    }
    //..........................................................................

    ///   Stash  To  Hash.......................................................
    @GET
    @Path("/stashtohash/{stash_id}")// Ekhane muloto lat long asbe +location id o aste pare .....tar upore depend kore information asbe....!
    @Produces(MediaType.APPLICATION_JSON)
    public String StashToHash(@PathParam("stash_id") String stash_id) {
        VotingDataModel vote = null;
        ResultSet rs = null;

        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            Connection con = null;
           con = (Connection) DriverManager.getConnection("jdbc:mysql://139.59.74.201/hashorstash", "tanvir", "De_pw9");

            String query = "update hashes_and_stashes set hashOrStash = ? where id = ?";
            PreparedStatement preparedStmt = con.prepareStatement(query);
            preparedStmt.setString(1, "hash");
            preparedStmt.setInt(2, Integer.parseInt(stash_id));
            preparedStmt.executeUpdate();
            con.close();
        } catch (Exception e) {
            return new Gson().toJson("failed");
        }
        return new Gson().toJson("success");
    }

    private void createFolderIfNotExists(String dirName)
            throws SecurityException {
        File theDir = new File(dirName);
        if (!theDir.exists()) {
            theDir.mkdir();
        }
    }
}
