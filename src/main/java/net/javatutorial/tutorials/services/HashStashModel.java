/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.javatutorial.tutorials.services;

import java.util.Date;

/**
 *
 * @author ATIK
 */
public class HashStashModel {
     private String userId, comments, location, locationId, latitute, longitute,  hashOrStash,image;
     private int rowId,duration,votecount;
     
 private long cmtTime;

    public int getVotecount() {
        return votecount;
    }

    public void setVotecount(int votecount) {
        this.votecount = votecount;
    }
    public HashStashModel() {
    }
     
     

    public HashStashModel(String userId, String comments, long cmtTime, String location, String locationId, String latitute, String longitute,String image, int duration, String hashOrStash, int rowId) {
        this.userId = userId;
        this.comments = comments;
        this.cmtTime = cmtTime;
        this.location = location;
        this.locationId = locationId;
        this.latitute = latitute;
        this.longitute = longitute;
        this.image=image;
        this.duration = duration;
        this.hashOrStash = hashOrStash;
        this.rowId = rowId;
    }

    

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public long getCmtTime() {
        return cmtTime;
    }

    public void setCmtTime(long cmtTime) {
        this.cmtTime = cmtTime;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public String getLatitute() {
        return latitute;
    }

    public void setLatitute(String latitute) {
        this.latitute = latitute;
    }

    public String getLongitute() {
        return longitute;
    }

    public void setLongitute(String longitute) {
        this.longitute = longitute;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getHashOrStash() {
        return hashOrStash;
    }

    public void setHashOrStash(String hashOrStash) {
        this.hashOrStash = hashOrStash;
    }

    public int getRowId() {
        return rowId;
    }

    public void setRowId(int rowId) {
        this.rowId = rowId;
    }
     
     
     
}
