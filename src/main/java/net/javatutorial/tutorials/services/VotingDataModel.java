/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.javatutorial.tutorials.services;

/**
 *
 * @author ATIK
 */
public class VotingDataModel {
         private String hashId, userVoteId;

    public VotingDataModel() {
    }

    public VotingDataModel(String hashId, String userId) {
        this.hashId = hashId;
        this.userVoteId = userId;
    }

    public String getHashId() {
        return hashId;
    }

    public void setHashId(String hashId) {
        this.hashId = hashId;
    }

    public String getUserVoteId() {
        return userVoteId;
    }

    public void setUserVoteId(String userVoteId) {
        this.userVoteId = userVoteId;
    }

    
         
         

}
